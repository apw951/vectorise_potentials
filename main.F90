Module Foo
        Implicit None

        Type, Public :: energy 
                Real, Dimension(1:2) :: ede
        End Type

        Type, Abstract, Public :: abstract_bar
                Contains
                        Procedure(baz), Deferred :: baz 
        End Type

        Type, Public :: bar_holder
                Class(abstract_bar), Allocatable :: bar 
                Contains 
                        Procedure, Public :: holder_baz
        End Type

        Abstract Interface
                Pure Type(energy) Function baz(b, r)
                        Import energy, abstract_bar
                        Class(abstract_bar), Intent(In   ) :: b
                        Real, Intent(In   ) :: r
                End Function
        End Interface

        Type, Extends(abstract_bar), Public :: concrete_bar
                Real :: eps = 1.0, sigma = 1.0
                Contains
                        Procedure :: baz => baz_concrete
        End Type

        Contains 

                        Pure Type(energy) Function baz_concrete(b, r)
                                Class(concrete_bar), Intent(In   ) :: b
                                Real, Intent(In   ) :: r
                                Real :: sor6
                                sor6 = (b%sigma/r)**6
                                baz_concrete%ede(1) = 4.0 * b%eps * sor6 * (sor6 - 1.0)
                                baz_concrete%ede(2) = 24.0 * b%eps * sor6 * (2.0 * sor6 - 1.0)
                        End Function

                        Elemental Type(energy) Function holder_baz(b, r)
                                Class(bar_holder), Intent(In   ) :: b 
                                Real, Intent(In   ) :: r 
                                holder_baz = b%bar%baz(r)
                        End Function
                        
                        Pure Type(energy) Function baz_free(re)
                                Real, Dimension(1:3), Intent(In   ) :: re
                                Real :: sor6
                                sor6 = (re(1)/re(3))**6
                                baz_free%ede(1) = 4.0 * re(2) * sor6 * (sor6 - 1.0)
                                baz_free%ede(2) = 24.0 * re(2) * sor6 * (2.0 * sor6 - 1.0)
                        End Function
End Module

Program Main
        Use Foo
        Implicit None

        Integer, Parameter :: N = 10000000
        Integer :: i
        Class(bar_holder), Allocatable :: bars(:)
        Real, Allocatable :: r(:)
        Type(energy), Allocatable :: eng(:)
        Class(abstract_bar), Allocatable :: bar
        Type(concrete_bar) :: concrete

        Real :: start, finish

        Allocate(bars(1:N))
        Allocate(r(1:N))
        Allocate(eng(1:N))

        Allocate(concrete_bar::bar)

        Do i = 1, N
                r(i) = Real(i)
                Allocate(concrete_bar::bars(i)%bar)
        End Do

        Call cpu_time(start)
        Do i = 1, N ! clobber
                eng(i) = bars(i)%holder_baz(r(i))
        End Do
        Call cpu_time(finish)
        Print *, "Via holder", finish-start

        Call cpu_time(start)
        Do i = 1, N ! more than one data ref
                eng(i) = bars(i)%bar%baz(r(i))
        End Do
        Call cpu_time(finish)
        Print *, "Skip holder", finish-start

        Call cpu_time(start)
        Do i = 1, N ! more than one data ref
                eng(i) = bar%baz(r(i))
        End Do
        Call cpu_time(finish)
        Print *, "Direct abstract", finish-start

        Call cpu_time(start)
        Do i = 1, N ! vectorises
                eng(i) = concrete%baz(r(i)) 
        End Do
        Call cpu_time(finish)
        Print *, "concrete (vectorised)", finish-start

        Print *, eng(1)
End Program

